﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scilab_Component;
using System.Runtime.InteropServices;
//=============================================================================

namespace Scilab_COM_server
{
    [Guid("FD29A67C-7321-4cf8-AA17-987AF0D2FA14"), 
    InterfaceType(ComInterfaceType.InterfaceIsDual)] 
    public interface Interface_COM_Scilab
    {
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Boolean isVisibleMainWindow();
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newState"></param>
        void setVisibleMainWindow(Boolean newState);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        int sendScilabJob(string command);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptFilename"></param>
        /// <returns></returns>
        int executeScript(string scriptFilename);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int getLastErrorCode();
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        void clearAllVariables();
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="varName"></param>
        /// <returns></returns>
        Boolean clearVariable(string varName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Boolean haveGraphicWindows();
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        int[] getNamedVarDimension(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixString"></param>
        /// <returns></returns>
        int createNamedMatrixOfString(string matrixName,
            int iRows, int iCols,
            string[] matrixString);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        string[] readNamedMatrixOfString(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedStringType(String matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="matrixString"></param>
        /// <returns></returns>
        Boolean createNamedSingleString(string matrixName, 
            string matrixString);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        String getNamedSingleString(String matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        int getNamedVarType(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedVarComplex(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedVarMatrixType(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean createNamedEmptyMatrix(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixBoolean"></param>
        /// <returns></returns>
        int createNamedMatrixOfBoolean(string matrixName,
            int iRows, int iCols,
            Boolean[] matrixBoolean);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean[] readNamedMatrixOfBoolean(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean getNamedScalarBoolean(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="scalarBoolean"></param>
        /// <returns></returns>
        Boolean createNamedScalarBoolean(string matrixName, Boolean scalarBoolean);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedVarExist(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedRowVector(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedColumnVector(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedVector(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedScalar(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedSquareMatrix(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedEmptyMatrix(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedPointerType(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixDouble"></param>
        /// <returns></returns>
        int createNamedMatrixOfDouble(string matrixName,
            int iRows, int iCols,
            double[] matrixDouble);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixDoubleReal"></param>
        /// <param name="matrixDoubleImg"></param>
        /// <returns></returns>
        int createNamedComplexMatrixOfDouble(string matrixName,
            int iRows, int iCols,
            double[] matrixDoubleReal, double[] matrixDoubleImg);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double[] readNamedMatrixOfDouble(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double[] readNamedComplexMatrixOfDoubleRealPart(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double[] readNamedComplexMatrixOfDoubleImgPart(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedDoubleType(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double getNamedScalarDouble(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double getNamedScalarComplexDoubleRealPart(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        double getNamedScalarComplexDoubleImgPart(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scalarName"></param>
        /// <param name="dblValue"></param>
        /// <returns></returns>
        Boolean createNamedScalarDouble(string scalarName,
            double dblValue);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scalarName"></param>
        /// <param name="dblRealPartValue"></param>
        /// <param name="dblImgPartValue"></param>
        /// <returns></returns>
        Boolean createNamedScalarComplexDouble(string scalarName,
            double dblRealPartValue, double dblImgPartValue);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt8"></param>
        /// <returns></returns>
        int createNamedMatrixOfInteger8(string matrixName,
            int iRows, int iCols,
            sbyte[] matrixInt8);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixUInt8"></param>
        /// <returns></returns>
        int createNamedMatrixOfUnsignedInteger8(string matrixName,
            int iRows, int iCols,
            byte[] matrixUInt8);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt16"></param>
        /// <returns></returns>
        int createNamedMatrixOfInteger16(string matrixName,
            int iRows, int iCols,
            Int16[] matrixInt16);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt32"></param>
        /// <returns></returns>
        int createNamedMatrixOfInteger32(string matrixName,
            int iRows, int iCols,
            Int32[] matrixInt32);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixUInt32"></param>
        /// <returns></returns>
        int createNamedMatrixOfUnsignedInteger32(
            string matrixName,
            int iRows, int iCols,
            UInt32[] matrixUInt32);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        int getNamedMatrixOfIntegerPrecision(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        sbyte[] readNamedMatrixOfInteger8(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        byte[] readNamedMatrixOfUnsignedInteger8(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Int16[] readNamedMatrixOfInteger16(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        UInt16[] readNamedMatrixOfUnsignedInteger16(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Int32[] readNamedMatrixOfInteger32(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        UInt32[] readNamedMatrixOfUnsignedInteger32(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Boolean isNamedIntegerType(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        sbyte getNamedScalarInteger8(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Int16 getNamedScalarInteger16(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        Int32 getNamedScalarInteger32(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        byte getNamedScalarUnsignedInteger8(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        UInt16 getNamedScalarUnsignedInteger16(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        UInt32 getNamedScalarUnsignedInteger32(string matrixName);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_cData"></param>
        /// <returns></returns>
        Boolean createNamedScalarInteger8(string matrixName, sbyte _cData);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_sData"></param>
        /// <returns></returns>
        Boolean createNamedScalarInteger16(string matrixName, Int16 _sData);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_iData"></param>
        /// <returns></returns>
        Boolean createNamedScalarInteger32(string matrixName, Int32 _iData);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_ucData"></param>
        /// <returns></returns>
        Boolean createNamedScalarUnsignedInteger8(string matrixName, byte _ucData);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_usData"></param>
        /// <returns></returns>
        Boolean createNamedScalarUnsignedInteger16(string matrixName, UInt16 _usData);
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="_uiData"></param>
        /// <returns></returns>
        Boolean createNamedScalarUnsignedInteger32(string matrixName, UInt32 _uiData);
        //=============================================================================
    }
    //=============================================================================
    [Guid("57D9917C-5422-4d66-ABD7-4728390EF4E2")]
    public class IScilab : Interface_COM_Scilab
    {
        //=============================================================================
        private static Scilab_API scilabObject = new Scilab_API();
        //=============================================================================
        public Boolean isVisibleMainWindow()
        {
            return scilabObject.isVisibleMainWindow();
        }
        //=============================================================================
        public void setVisibleMainWindow(Boolean newState)
        {
            scilabObject.setVisibleMainWindow(newState);
        }
        //=============================================================================
        public int executeScript(string scriptFilename)
        {
            return scilabObject.executeScript(scriptFilename);
        }
        //=============================================================================
        public int sendScilabJob(string command)
        {
            return scilabObject.sendScilabJob(command);
        }
        //=============================================================================
        public void clearAllVariables()
        {
            scilabObject.clearAllVariables();
        }
        //=============================================================================
        public Boolean clearVariable(string matrixName)
        {
            return scilabObject.clearVariable(matrixName);
        }
        //=============================================================================
        public int getLastErrorCode()
        {
            return scilabObject.getLastErrorCode();
        }
        //=============================================================================
        public Boolean haveGraphicWindows()
        {
            return scilabObject.haveGraphicWindows();
        }
        //=============================================================================
        public int[] getNamedVarDimension(string matrixName)
        {
            return scilabObject.getNamedVarDimension(matrixName);
        }
        //=============================================================================
        public int createNamedMatrixOfString(string matrixName,
            int iRows, int iCols,
            string[] matrixString)
        {
            return scilabObject.createNamedMatrixOfString(matrixName,
            iRows, iCols,
            matrixString);
        }
        //=============================================================================
        public string[] readNamedMatrixOfString(string matrixName)
        {
            return scilabObject.readNamedMatrixOfString(matrixName);
        }
        //=============================================================================
        public Boolean isNamedStringType(String matrixName)
        {
            return scilabObject.isNamedStringType(matrixName);
        }
        //=============================================================================
        public Boolean createNamedSingleString(string matrixName, string matrixString)
        {
            return scilabObject.createNamedSingleString(matrixName, matrixString);
        }
        //=============================================================================
        public String getNamedSingleString(String matrixName)
        {
            return scilabObject.getNamedSingleString(matrixName);
        }
        //=============================================================================
        public int getNamedVarType(string matrixName)
        {
            return scilabObject.getNamedVarType(matrixName);
        }
        //=============================================================================
        public Boolean isNamedVarComplex(string matrixName)
        {
            return scilabObject.isNamedVarComplex(matrixName);
        }
        //=============================================================================
        public Boolean isNamedVarMatrixType(string matrixName)
        {
            return scilabObject.isNamedVarMatrixType(matrixName);
        }
        //=============================================================================
        public Boolean createNamedEmptyMatrix(string matrixName)
        {
            return scilabObject.createNamedEmptyMatrix(matrixName);
        }
        //=============================================================================
        public int createNamedMatrixOfBoolean(string matrixName,
            int iRows, int iCols,
            Boolean[] matrixBoolean)
        {
            return scilabObject.createNamedMatrixOfBoolean(matrixName, 
                iRows, iCols, 
                matrixBoolean);
        }
        //=============================================================================
        public Boolean[] readNamedMatrixOfBoolean(string matrixName)
        {
            return scilabObject.readNamedMatrixOfBoolean(matrixName);
        }
        //=============================================================================
        public Boolean isNamedBooleanType(string matrixName)
        {
            return scilabObject.isNamedBooleanType(matrixName);
        }
        //=============================================================================
        public Boolean getNamedScalarBoolean(string matrixName)
        {
            return scilabObject.getNamedScalarBoolean(matrixName);
        }
        //=============================================================================
        public Boolean createNamedScalarBoolean(string matrixName, Boolean scalarBoolean)
        {
            return scilabObject.createNamedScalarBoolean(matrixName, scalarBoolean);
        }
        //=============================================================================
        public Boolean isNamedVarExist(string matrixName)
        {
            return scilabObject.isNamedVarExist(matrixName);
        }
        //=============================================================================
        public Boolean isNamedRowVector(string matrixName)
        {
            return scilabObject.isNamedRowVector(matrixName);
        }
        //=============================================================================
        public Boolean isNamedColumnVector(string matrixName)
        {
            return scilabObject.isNamedColumnVector(matrixName);
        }
        //=============================================================================
        public Boolean isNamedVector(string matrixName)
        {
            return scilabObject.isNamedVector(matrixName);
        }
        //=============================================================================
        public Boolean isNamedScalar(string matrixName)
        {
            return scilabObject.isNamedScalar(matrixName);
        }
        //=============================================================================
        public Boolean isNamedSquareMatrix(string matrixName)
        {
            return scilabObject.isNamedSquareMatrix(matrixName);
        }
        //=============================================================================
        public Boolean isNamedEmptyMatrix(string matrixName)
        {
            return scilabObject.isNamedEmptyMatrix(matrixName);
        }
        //=============================================================================
        public Boolean isNamedPointerType(string matrixName)
        {
            return scilabObject.isNamedPointerType(matrixName);
        }
        //=============================================================================
        public int createNamedMatrixOfDouble(string matrixName, 
            int iRows, int iCols, 
            double[] matrixDouble)
        {
            return scilabObject.createNamedMatrixOfDouble(matrixName,
            iRows, iCols,
            matrixDouble);
        }
        //=============================================================================
        public int createNamedComplexMatrixOfDouble(string matrixName, 
            int iRows, int iCols, 
            double[] matrixDoubleReal, double[] matrixDoubleImg)
        {
            return scilabObject.createNamedComplexMatrixOfDouble(matrixName,
            iRows, iCols,
            matrixDoubleReal, matrixDoubleImg);
        }
        //=============================================================================
        public double[] readNamedMatrixOfDouble(string matrixName)
        {
            return scilabObject.readNamedMatrixOfDouble(matrixName);
        }
        //=============================================================================
        public double[] readNamedComplexMatrixOfDoubleRealPart(string matrixName)
        {
            return scilabObject.readNamedComplexMatrixOfDoubleRealPart(matrixName);
        }
        //=============================================================================
        public double[] readNamedComplexMatrixOfDoubleImgPart(string matrixName)
        {
            return scilabObject.readNamedComplexMatrixOfDoubleImgPart(matrixName);
        }
        //=============================================================================
        public Boolean isNamedDoubleType(string matrixName)
        {
            return scilabObject.isNamedDoubleType(matrixName);
        }
        //=============================================================================
        public double getNamedScalarDouble(string matrixName)
        {
            return scilabObject.getNamedScalarDouble(matrixName);
        }
        //=============================================================================
        public double getNamedScalarComplexDoubleRealPart(string matrixName)
        {
            return scilabObject.getNamedScalarComplexDoubleRealPart(matrixName);
        }
        //=============================================================================
        public double getNamedScalarComplexDoubleImgPart(string matrixName)
        {
            return scilabObject.getNamedScalarComplexDoubleImgPart(matrixName);
        }
        //=============================================================================
        public Boolean createNamedScalarDouble(string scalarName,
            double dblValue)
        {
            return scilabObject.createNamedScalarDouble(scalarName,
                dblValue);
        }
        //=============================================================================
        public Boolean createNamedScalarComplexDouble(string scalarName, 
            double dblRealPartValue, double dblImgPartValue)
        {
            return scilabObject.createNamedScalarComplexDouble(scalarName,
                dblRealPartValue, dblImgPartValue);
        }
        //=============================================================================
        public int createNamedMatrixOfInteger8(string matrixName,
            int iRows, int iCols, 
            sbyte[] matrixInt8)
        {
            return scilabObject.createNamedMatrixOfInteger8(matrixName,
            iRows, iCols,
            matrixInt8);
        }
        //=============================================================================
        public int createNamedMatrixOfUnsignedInteger8(string matrixName, 
            int iRows, int iCols, 
            byte [] matrixUInt8)
        {
            return scilabObject.createNamedMatrixOfUnsignedInteger8(matrixName,
                iRows, iCols,
                matrixUInt8);
        }
        //=============================================================================
        public int createNamedMatrixOfInteger16(string matrixName,
            int iRows, int iCols,
            Int16[] matrixInt16)
        {
            return scilabObject.createNamedMatrixOfInteger16(matrixName,
                iRows, iCols,
                matrixInt16);
        }
        //=============================================================================
        public int createNamedMatrixOfUnsignedInteger16(string matrixName, 
            int iRows, int iCols,
            UInt16[] matrixUInt16)
        {
            return scilabObject.createNamedMatrixOfUnsignedInteger16(matrixName,
                iRows, iCols, matrixUInt16);
        }
        //=============================================================================
        public int createNamedMatrixOfInteger32(string matrixName, 
            int iRows, int iCols,
            Int32[] matrixInt32)
        {
            return scilabObject.createNamedMatrixOfInteger32(matrixName,
                iRows, iCols, matrixInt32);
        }
        //=============================================================================
        public int createNamedMatrixOfUnsignedInteger32(
            string matrixName, 
            int iRows, int iCols,
            UInt32[] matrixUInt32)
        {
            return scilabObject.createNamedMatrixOfUnsignedInteger32(
                matrixName,
                iRows, iCols,
                matrixUInt32);
        }
        //=============================================================================
        public int getNamedMatrixOfIntegerPrecision(string matrixName)
        {
            return scilabObject.getNamedMatrixOfIntegerPrecision(matrixName);
        }
        //=============================================================================
        public sbyte[] readNamedMatrixOfInteger8(string matrixName)
        {
            return scilabObject.readNamedMatrixOfInteger8(matrixName);
        }
        //=============================================================================
        public byte[] readNamedMatrixOfUnsignedInteger8(string matrixName)
        {
            return scilabObject.readNamedMatrixOfUnsignedInteger8(matrixName);
        }
        //=============================================================================
        public Int16[] readNamedMatrixOfInteger16(string matrixName)
        {
            return scilabObject.readNamedMatrixOfInteger16(matrixName);
        }
        //=============================================================================
        public UInt16[] readNamedMatrixOfUnsignedInteger16(string matrixName)
        {
            return scilabObject.readNamedMatrixOfUnsignedInteger16(matrixName);
        }
        //=============================================================================
        public Int32[] readNamedMatrixOfInteger32(string matrixName)
        {
            return scilabObject.readNamedMatrixOfInteger32(matrixName);
        }
        //=============================================================================
        public UInt32[] readNamedMatrixOfUnsignedInteger32(string matrixName)
        {
            return scilabObject.readNamedMatrixOfUnsignedInteger32(matrixName);
        }
        //=============================================================================
        public Boolean isNamedIntegerType(string matrixName)
        {
            return scilabObject.isNamedIntegerType(matrixName);
        }
        //=============================================================================
        public sbyte getNamedScalarInteger8(string matrixName)
        {
            return scilabObject.getNamedScalarInteger8(matrixName);
        }
        //=============================================================================
        public Int16 getNamedScalarInteger16(string matrixName)
        {
            return scilabObject.getNamedScalarInteger16(matrixName);
        }
        //=============================================================================
        public Int32 getNamedScalarInteger32(string matrixName)
        {
            return scilabObject.getNamedScalarInteger32(matrixName);
        }
        //=============================================================================
        public byte getNamedScalarUnsignedInteger8(string matrixName)
        {
            return scilabObject.getNamedScalarUnsignedInteger8(matrixName);
        }
        //=============================================================================
        public UInt16 getNamedScalarUnsignedInteger16(string matrixName)
        {
            return scilabObject.getNamedScalarUnsignedInteger16(matrixName);
        }
        //=============================================================================
        public UInt32 getNamedScalarUnsignedInteger32(string matrixName)
        {
            return scilabObject.getNamedScalarUnsignedInteger32(matrixName);
        }
        //=============================================================================
        public Boolean createNamedScalarInteger8(string matrixName,
                        sbyte _cData)
        {
            return scilabObject.createNamedScalarInteger8(matrixName, _cData);
        }
        //=============================================================================
        public Boolean createNamedScalarInteger16(string matrixName,
                        Int16 _sData)
        {
            return scilabObject.createNamedScalarInteger16(matrixName, _sData);
        }
        //=============================================================================
        public Boolean createNamedScalarInteger32(string matrixName,
                        Int32 _iData)
        {
            return scilabObject.createNamedScalarInteger32(matrixName, _iData);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger8(string matrixName,
                        byte _ucData)
        {
            return scilabObject.createNamedScalarUnsignedInteger8(matrixName, _ucData);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger16(string matrixName,
                        UInt16 _usData)
        {
            return scilabObject.createNamedScalarUnsignedInteger16(matrixName, _usData);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger32(string matrixName,
                        UInt32 _uiData)
        {
            return scilabObject.createNamedScalarUnsignedInteger32(matrixName, _uiData);
        }
        //=============================================================================
    }
    //=============================================================================
}
//=============================================================================
