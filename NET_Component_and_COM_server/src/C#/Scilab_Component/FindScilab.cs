﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
//=============================================================================
namespace Scilab_Component
{
    public class FindScilab
    {
        //=============================================================================
        /// <summary>
        /// returns Scilab x86 PATH from registry
        /// </summary>
        /// <returns> string Scilab x86 path</returns>
        private static String getScilabPathX86()
        {
            String ScilabPath = "";
            String LastInstall = getLastInstallScilab();
            if (LastInstall.CompareTo("") != 0) 
            {
                RegistryKey start = Registry.LocalMachine;
                RegistryKey ScilabPathRegistryKey;

                string ScilabKeyX86 = "SOFTWARE\\Scilab\\" + LastInstall;
                string ScilabKeyWow6432Node = "SOFTWARE\\Wow6432Node\\Scilab\\" + LastInstall;

                ScilabPathRegistryKey = start.OpenSubKey(ScilabKeyWow6432Node);
                if (ScilabPathRegistryKey == null)
                {
                    ScilabPathRegistryKey = start.OpenSubKey(ScilabKeyX86);
                    if (ScilabPathRegistryKey == null)
                    {
                        start.Close();
                        return ScilabPath;
                    }
                }

                ScilabPath = (string)ScilabPathRegistryKey.GetValue("SCIPATH");
                if (ScilabPath == null)
                {
                    ScilabPath = "";
                }
                start.Close();
            }
            return ScilabPath;
        }
        //=============================================================================
        /// <summary>
        /// returns Scilab x64 PATH from registry
        /// </summary>
        /// <returns> string Scilab x64 path</returns>
        private static String getScilabPathX64()
        {
            String ScilabPath = "";
            String LastInstall = getLastInstallScilab();
            if (LastInstall.CompareTo("") != 0)
            {
                RegistryKey start = Registry.LocalMachine;
                RegistryKey ScilabPathRegistryKey;
                string ScilabKeyX64 = "SOFTWARE\\Scilab\\" + LastInstall;

                ScilabPathRegistryKey = start.OpenSubKey(ScilabKeyX64);
                if (ScilabPathRegistryKey == null)
                {
                    start.Close();
                    return ScilabPath;
                }

                ScilabPath = (string)ScilabPathRegistryKey.GetValue("SCIPATH");
                if (ScilabPath == null)
                {
                    ScilabPath = "";
                }
                start.Close();
            }
            return ScilabPath;
        }
        //=============================================================================
        /// <summary>
        /// returns last install scilab x86 path from registry
        /// </summary>
        /// <returns>string last install x86 path</returns>
        private static String getLastInstallScilabX86()
        {
            RegistryKey start = Registry.LocalMachine;
            RegistryKey LastInstallRegistryKey;

            string lastInstallValue = "";
            string ScilabKeyX86 = "SOFTWARE\\Scilab\\";
            string ScilabKeyWow6432Node = "SOFTWARE\\Wow6432Node\\Scilab";

            LastInstallRegistryKey = start.OpenSubKey(ScilabKeyWow6432Node);

            if (LastInstallRegistryKey == null)
            {
                LastInstallRegistryKey = start.OpenSubKey(ScilabKeyX86);
                if (LastInstallRegistryKey == null)
                {
                    start.Close();
                    return lastInstallValue;
                }
            }

            lastInstallValue = (string)LastInstallRegistryKey.GetValue("LASTINSTALL");
            if (lastInstallValue == null)
            {
                lastInstallValue = "";
            }
            start.Close();

            return lastInstallValue;
        
        }
        //=============================================================================
        /// <summary>
        /// returns last install scilab x64 path from registry
        /// </summary>
        /// <returns>string last install x64 path</returns>
        private static String getLastInstallScilabX64()
        {
            RegistryKey start = Registry.LocalMachine;
            RegistryKey LastInstallRegistryKey;

            string lastInstallValue = "";
            string ScilabKeyX64 = "SOFTWARE\\Scilab\\";

            LastInstallRegistryKey = start.OpenSubKey(ScilabKeyX64);

            if (LastInstallRegistryKey == null)
            {
                start.Close();
                return lastInstallValue;
            }

            lastInstallValue = (string)LastInstallRegistryKey.GetValue("LASTINSTALL");
            if (lastInstallValue == null)
            {
                lastInstallValue = "";
            }
            start.Close();

            return lastInstallValue;
        }
        //=============================================================================
        /// <summary>
        /// returns last install scilab directory from registry
        /// </summary>
        /// <returns>string last install scilab directory</returns>
        private static String getLastInstallScilab()
        {
            if (isX64())
            {
                return getLastInstallScilabX64();
            }
            else
            {
                return getLastInstallScilabX86();
            }
        }
        //=============================================================================
        /// <summary>
        /// get Scilab path from registry
        /// </summary>
        /// <returns>string scilab path detected</returns>
        public static String getScilabPath()
        {
            if (isX64())
            {
                return getScilabPathX64();
            }
            else
            {
                return getScilabPathX86();
            }
        }
        //=============================================================================
        private static bool isX64()
        {
            bool bX64 = false;
            if (IntPtr.Size == 8)
            {
                bX64 = true;
            }
            return bX64;
        }
        //=============================================================================
    }
}
//=============================================================================
